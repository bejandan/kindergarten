import React from 'react';
import { Input, Button } from 'antd';

export default class AddChild extends React.Component {
  render() {
    return (
      <div className="add-child">
        <Input
          placeholder="Type a child fullname"
          type={Input.Text}
          disabled={false}
          ref={input => (this.addChildInput = input)}
        />
        <Button
          type="primary"
          onClick={() => {
            this.props.onAddChild(this.addChildInput.input.value);
            this.addChildInput.input.value = '';
          }}
        >
          Add Child
        </Button>
      </div>
    );
  }
}
