import React from 'react';
import { GroupItem } from './GroupItem';

export const GroupList = props => (
  <div className="group-list">
    {props.groups.map(group => {
      return (
        <GroupItem
          item={group}
          key={group.id}
          childList={props.allChildren.filter(
            child => child.groupId === group.id
          )}
          onAddChild={(groupID, childName) =>
            props.onAddChild(groupID, childName)
          }
          onRemoveChild={child => props.onRemoveChild(child)}
          onEditGroup={newGroupName => props.onEditGroup(group, newGroupName)}
          onDeleteGroup={() => props.onDeleteGroup(group)}
          onUpdateChild={(child, newName) =>
            props.onUpdateChild(child, newName)
          }
          onUpdateChecked={(child, value) =>
            props.onUpdateChecked(child, value)
          }
        />
      );
    })}
  </div>
);
