import React from 'react';
import { Input, Button } from 'antd';

const style = {
  margin: 5
};

export default class DeleteEditGroup extends React.Component {
  render() {
    return (
      <div className="delete-edit-group">
        <Input
          placeholder="Type a new group name"
          type={Input.Text}
          disabled={false}
          ref={input => (this.editGroupInput = input)}
        />
        <Button
          type="primary"
          onClick={() => {
            this.props.onEditGroup(this.editGroupInput.input.value);
            this.editGroupInput.input.value = '';
          }}
          style={style}
        >
          Save
        </Button>
        <Button
          type="danger"
          onClick={() => {
            this.props.onDeleteGroup();
          }}
          style={style}
        >
          Delete
        </Button>
      </div>
    );
  }
}
