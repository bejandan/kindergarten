import React from 'react';
import { Radio, ListItem, TextField, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { BLUE, BLACK, RED } from '../constants/type-filters';

export default class ChildItem extends React.Component {
  state = {
    checked: this.props.item.checked,
    fullname: this.props.item.fullname
  };

  handleChange = async event => {
    const { value } = event.target;
    this.setState({ checked: value });
    this.props.onUpdateChecked(this.props.item, value);
  };

  handleNameChange = fullname => event => {
    this.setState({
      [fullname]: event.target.value
    });
  };

  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.props.onUpdateChild(this.props.item, this.state.fullname);
    }
  };

  render() {
    const { item } = this.props;
    return (
      <ListItem>
        <TextField
          id="fullname"
          label="Fullname"
          value={this.state.fullname}
          onChange={this.handleNameChange('fullname')}
          onKeyPress={this.handleKeyPress}
          margin="normal"
        />
        <Radio
          checked={this.state.checked === BLUE}
          onChange={this.handleChange}
          value={BLUE}
          name="radio-button-demo"
          aria-label="A"
          color="primary"
        />
        <Radio
          checked={this.state.checked === RED}
          onChange={this.handleChange}
          value={RED}
          name="radio-button-demo"
          aria-label="B"
          color="secondary"
        />
        <Radio
          checked={this.state.checked === BLACK}
          onChange={this.handleChange}
          value={BLACK}
          name="radio-button-demo"
          aria-label="C"
          color="default"
        />
        <IconButton
          aria-label="delete"
          onClick={() => this.props.onRemoveChild(item)}
        >
          <DeleteIcon />
        </IconButton>
      </ListItem>
    );
  }
}
