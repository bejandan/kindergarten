import React from 'react';
import ChildItem from './ChildItem';
import {
  List,
  InputLabel,
  FormControl,
  Select,
  MenuItem
} from '@material-ui/core';

import {
  ALL,
  BLACK,
  BLUE,
  RED,
  childFilterMapper
} from '../constants/type-filters';

const style = {
  margin: 1,
  minWidth: 120
};

export default class ChildList extends React.Component {
  state = {
    filter: ALL
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { childList } = this.props;
    const { filter: currentFilter } = this.state;
    return (
      <div className="children-list">
        <form autoComplete="off">
          <FormControl style={style}>
            <InputLabel htmlFor="filter">Filter by</InputLabel>
            <Select
              value={this.state.filter}
              onChange={this.handleChange}
              inputProps={{
                name: 'filter'
              }}
            >
              <MenuItem value={ALL} selected={true}>
                All
              </MenuItem>

              <MenuItem value={RED}>Red</MenuItem>
              <MenuItem value={BLUE}>Blue</MenuItem>
              <MenuItem value={BLACK}>Black</MenuItem>
            </Select>
          </FormControl>
        </form>
        <List>
          {childList
            .filter(childFilterMapper[currentFilter])
            .map(child => (
              <ChildItem
                key={child.id}
                item={child}
                onRemoveChild={child => this.props.onRemoveChild(child)}
                onUpdateChild={(child, newName) =>
                  this.props.onUpdateChild(child, newName)
                }
                onUpdateChecked={(child, value) =>
                  this.props.onUpdateChecked(child, value)
                }
              />
            ))}
        </List>
      </div>
    );
  }
}
