import React from 'react';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChildList from './ChildList';
import AddChild from './AddChild';
import DeleteEditGroup from './DeleteEditGroup';

export const GroupItem = props => (
  <div className="group-item">
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>Group: {props.item.name}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails classes={{ root: 'block' }}>
        <DeleteEditGroup
          id={props.item.id}
          onEditGroup={newGroupName => props.onEditGroup(newGroupName)}
          onDeleteGroup={() => props.onDeleteGroup()}
        />

        <AddChild
          onAddChild={childName => props.onAddChild(props.item.id, childName)}
        />
        {props.childList.length > 0 ? (
          <ChildList
            childList={props.childList}
            onRemoveChild={child => props.onRemoveChild(child)}
            onUpdateChild={(child, newName) =>
              props.onUpdateChild(child, newName)
            }
            onUpdateChecked={(child, value) =>
              props.onUpdateChecked(child, value)
            }
          />
        ) : (
          <div>No children enrolled..</div>
        )}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  </div>
);
