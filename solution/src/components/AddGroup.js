import React from 'react';
import { Input, Button } from 'antd';

export default class AddGroup extends React.Component {
  render() {
    return (
      <div className="add-group">
        <Input
          placeholder="Type a group name"
          type={Input.Text}
          disabled={false}
          ref={input => (this.addGroupInput = input)}
        />
        <Button
          type="primary"
          onClick={() => {
            this.props.onAddGroup(this.addGroupInput.input.value);
            this.addGroupInput.input.value = '';
          }}
        >
          Add Group
        </Button>
      </div>
    );
  }
}
