import React, { Component } from 'react';
import { GroupList } from './components/GroupList';
import AddGroup from './components/AddGroup';
import uuidv1 from 'uuid/v1';
import { BLUE } from './constants/type-filters';
import { notification } from 'antd';
import { headers } from './constants/headers';
import './css/app.css';
import 'antd/dist/antd.css';

const openNotification = (message, description) => {
  notification.open({
    message,
    description
  });
};

class App extends Component {
  state = {
    groups: [],
    children: []
  };

  getGroups = async () => {
    const groupsResponse = await fetch('http://localhost:3000/groups', {
      headers,
      method: 'GET'
    });
    const groups = await groupsResponse.json();
    return groups;
  };

  getChildren = async () => {
    const childrenResponse = await fetch('http://localhost:3000/children', {
      headers,
      method: 'GET'
    });
    const children = await childrenResponse.json();
    return children;
  };

  async componentDidMount() {
    const groups = await this.getGroups();
    const children = await this.getChildren();
    this.setState({
      groups,
      children
    });
  }

  handleAddGroup = async name => {
    const group = { id: uuidv1(), name };
    const response = await fetch('http://localhost:3000/groups', {
      headers,
      body: JSON.stringify(group),
      method: 'POST'
    });
    const addedGroup = await response.json();
    this.setState(
      prevState => ({ groups: [...prevState.groups, addedGroup] }),
      () => {
        openNotification('Group', `The group ${name} was added succesfully`);
      }
    );
  };

  handleAddChild = async (groupID, childName) => {
    const child = {
      id: uuidv1(),
      groupId: groupID,
      fullname: childName,
      checked: BLUE
    };
    const response = await fetch(`http://localhost:3000/children`, {
      headers,
      body: JSON.stringify(child),
      method: 'POST'
    });
    const addedChild = await response.json();
    this.setState(
      prevState => ({
        children: [...prevState.children, addedChild]
      }),
      () => {
        openNotification(
          'Child',
          `The child ${childName} was added succesfully`
        );
      }
    );
  };

  handleRemoveChild = async childToDelete => {
    const response = await fetch(
      `http://localhost:3000/children/${childToDelete.id}`,
      {
        headers,
        method: 'DELETE'
      }
    );
    if (response.status === 200) {
      this.setState(
        prevState => {
          return {
            children: prevState.children.filter(
              child => child.id !== childToDelete.id
            )
          };
        },
        () => {
          openNotification(
            'Child',
            `The child ${childToDelete.fullname} was removed succesfully`
          );
        }
      );
    }
  };

  handleEditGroup = async (groupToEdit, newGroupName) => {
    const editedGroup = { ...groupToEdit, name: newGroupName };
    const response = await fetch(
      `http://localhost:3000/groups/${groupToEdit.id}`,
      {
        headers,
        body: JSON.stringify(editedGroup),
        method: 'PUT'
      }
    );
    if (response.status === 200) {
      this.setState(
        prevState => {
          return {
            groups: prevState.groups.map(group => {
              if (group.id === editedGroup.id) {
                return editedGroup;
              }
              return group;
            })
          };
        },
        () => {
          openNotification(
            'Group',
            `Group ${groupToEdit.name} changed to ${editedGroup.name}`
          );
        }
      );
    }
  };

  handleDeleteGroup = async groupToDelete => {
    const response = await fetch(
      `http://localhost:3000/groups/${groupToDelete.id}`,
      {
        headers,
        method: 'DELETE'
      }
    );

    if (response.status === 200) {
      this.setState(
        prevState => {
          return {
            groups: prevState.groups.filter(
              group => group.id !== groupToDelete.id
            ),
            children: prevState.children.filter(
              child => child.groupId !== groupToDelete.id
            )
          };
        },
        () => {
          openNotification(
            'Group',
            `The group ${groupToDelete.name} was removed succesfully`
          );
        }
      );
    }
  };

  handleUpdateChild = async (childToUpdate, newName) => {
    const updatedChild = { ...childToUpdate, fullname: newName };
    const response = await fetch(
      `http://localhost:3000/children/${childToUpdate.id}`,
      {
        headers,
        body: JSON.stringify(updatedChild),
        method: 'PUT'
      }
    );
    if (response.status === 200) {
      this.setState(
        prevState => {
          return {
            children: prevState.children.map(child => {
              if (child.id === childToUpdate.id) {
                return childToUpdate;
              }
              return child;
            })
          };
        },
        () => {
          openNotification(
            'Children',
            `Child ${childToUpdate.fullname} name changed to ${
              updatedChild.fullname
            }`
          );
        }
      );
    }
  };

  handleChecked = async (childToUpdate, newValue) => {
    const updatedChild = { ...childToUpdate, checked: newValue };
    const response = await fetch(
      `http://localhost:3000/children/${childToUpdate.id}`,
      {
        headers,
        body: JSON.stringify(updatedChild),
        method: 'PUT'
      }
    );
    if (response.status === 200) {
      this.setState(prevState => {
        return {
          children: prevState.children.map(child => {
            if (child.id === childToUpdate.id) {
              return updatedChild;
            }
            return child;
          })
        };
      });
    }
  };

  render() {
    const { groups, children: allChildren } = this.state;
    return (
      <div className="App">
        <AddGroup onAddGroup={this.handleAddGroup} />
        {groups.length > 0 ? (
          <GroupList
            groups={groups}
            allChildren={allChildren}
            onAddChild={this.handleAddChild}
            onRemoveChild={this.handleRemoveChild}
            onEditGroup={this.handleEditGroup}
            onDeleteGroup={this.handleDeleteGroup}
            onUpdateChild={this.handleUpdateChild}
            onUpdateChecked={this.handleChecked}
          />
        ) : (
          <div>Loading data..</div>
        )}
      </div>
    );
  }
}

export default App;
