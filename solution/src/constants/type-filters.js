export const ALL = 'ALL';
export const BLACK = 'BLACK';
export const RED = 'RED';
export const BLUE = 'BLUE';

export const childFilterMapper = {
  [ALL]: () => true,
  [BLACK]: child => child.checked === BLACK,
  [BLUE]: child => child.checked === BLUE,
  [RED]: child => child.checked === RED
};
